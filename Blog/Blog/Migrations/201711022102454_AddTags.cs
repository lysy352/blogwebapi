namespace BlogWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTags : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.TagName);
            
            CreateTable(
                "dbo.PostsTags",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        TagName = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.PostId, t.TagName })
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagName, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.TagName);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostsTags", "TagName", "dbo.Tags");
            DropForeignKey("dbo.PostsTags", "PostId", "dbo.Posts");
            DropIndex("dbo.PostsTags", new[] { "TagName" });
            DropIndex("dbo.PostsTags", new[] { "PostId" });
            DropTable("dbo.PostsTags");
            DropTable("dbo.Tags");
        }
    }
}
