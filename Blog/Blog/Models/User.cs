﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogWebApi.Models
{
    public class User
    {
        [Key]
        public String Username { get; set; }
        public String Description { get; set; }
    }
}