﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace BlogWebApi.Models
{
    public class BlogContext : DbContext
    {
        public BlogContext() : base("BlogDB") 
        {

        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Post>()
               .HasMany<Tag>(p => p.Tags)
               .WithMany(t => t.Posts)
               .Map(pt =>
               {
                   pt.MapLeftKey("PostId");
                   pt.MapRightKey("TagName");
                   pt.ToTable("PostsTags");
               });

            base.OnModelCreating(modelBuilder);

        }
    }
}