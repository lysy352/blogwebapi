﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogWebApi.Models
{
    public class Comment
    {
        public int CommentId { get; set; }

        [Required]
        public DateTime Date { get; set; }
        [Required]
        public String Content { get; set; }

        [ForeignKey("Author"), Required]
        public String AuthorName { get; set; }

        [JsonIgnore]
        public virtual User Author { get; set; }

        
        [ForeignKey("ParentComment")]
        public int? ParentCommentId { get; set; }

        [JsonIgnore]
        public virtual Comment ParentComment { get; set; }

        [InverseProperty("ParentComment")]
        public ICollection<Comment> Replies { get; set; }
    }
}