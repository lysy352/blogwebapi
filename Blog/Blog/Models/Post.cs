﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlogWebApi.Models
{
    public class Post
    {
        public int PostId { get; set; }

        [Required]
        public String Title { get; set; }
        public String Content { get; set; }

        [Required]
        public DateTime Date { get; set; }        

        [ForeignKey("Author"), Required]
        public String AuthorName { get; set; }

        [JsonIgnore]
        public virtual User Author { get; set; }

        [JsonIgnore]
        public virtual ICollection<Comment> Comments { get; set; }   
        

        public ICollection<Tag> Tags { get; set; }


    }
}