﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogWebApi.Models
{
    public class Tag
    {
        [Key]
        public String TagName { get; set; }

        [JsonIgnore]
        public ICollection<Post> Posts { get; set; }
    }
}