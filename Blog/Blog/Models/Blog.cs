﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogWebApi.Models
{
    public class Blog
    {
        public int BlogId { get; set; }

        [Required]
        public String Title { get; set; }
        public String Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<Post> Posts { get; set; }        
    }
}