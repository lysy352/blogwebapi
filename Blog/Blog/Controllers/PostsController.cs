﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BlogWebApi.Models;

namespace BlogWebApi.Controllers
{
    public class PostsController : ApiController
    {
        private BlogContext db = new BlogContext();
        private const int COMMENTS_ON_PAGE = 10;

        // GET: api/Posts
        public IQueryable<Post> GetPosts()
        {
            return db.Posts;
        }

        // GET: api/Posts/5
        [ResponseType(typeof(Post))]
        public IHttpActionResult GetPost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }

        [Route("api/Posts/{postId}/Comments")]
        public IEnumerable<Comment> GetPostComments(int postId)
        {
            return GetPostCommentsByPage(postId, 1);                
        }

        [Route("api/Posts/{postId}/Comments/page/{pageNumber}")]
        public IEnumerable<Comment> GetPostCommentsByPage(int postId, int pageNumber)
        {
            return (from comment in (from post in db.Posts
                             where post.PostId == postId
                             select post.Comments).First()
            orderby comment.Date descending
            select comment).Skip((pageNumber - 1) * COMMENTS_ON_PAGE).Take(COMMENTS_ON_PAGE);            
        }

        [Route("api/Posts/{postId}/Comments/count")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetCommentsNumber(int postId)
        {
            if (!PostExists(postId))
            {
                return NotFound();
            }

            return Ok((from post in db.Posts
                       where post.PostId == postId
                       select post.Comments).Count());                
                      
        }

        [Route("api/Posts/tag/{tagName}")]
        public IEnumerable<Post> GetCommentsNumber(string tagName)
        {
            return db.Tags.Where(t => t.TagName == tagName).First().Posts;
        }

        // PUT: api/Posts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPost(int id, Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != post.PostId)
            {
                return BadRequest();
            }

            db.Entry(post).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Posts
        [ResponseType(typeof(Post))]
        public IHttpActionResult PostPost(Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Posts.Add(post);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = post.PostId }, post);
        }

        // DELETE: api/Posts/5
        [ResponseType(typeof(Post))]
        public IHttpActionResult DeletePost(int id)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);
            db.SaveChanges();

            return Ok(post);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PostExists(int id)
        {
            return db.Posts.Count(e => e.PostId == id) > 0;
        }
    }
}