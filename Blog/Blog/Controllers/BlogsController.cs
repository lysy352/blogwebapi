﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BlogWebApi.Models;

namespace BlogWebApi.Controllers
{
    public class BlogsController : ApiController
    {
        private BlogContext db = new BlogContext();
        private const int POSTS_ON_PAGE = 10;

        // GET: api/Blogs
        public IQueryable<Blog> GetBlogs()
        {
            return db.Blogs;
        }

        // GET: api/Blogs/5
        [ResponseType(typeof(Blog))]
        public IHttpActionResult GetBlog(int id)
        {
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return NotFound();
            }

            return Ok(blog);
        }

        [Route("api/Blogs/{blogId}/Posts")]
        public IEnumerable<Post> GetBlogPosts(int blogId)
        {
            return GetBlogPostsByPage(blogId, 1);                                
        }

        [Route("api/Blogs/{blogId}/Posts/page/{pageNumber}")]
        public IEnumerable<Post> GetBlogPostsByPage(int blogId, int pageNumber)
        {            
            return db.Blogs
                .Where(b => b.BlogId == blogId)
                .First()
                .Posts
                .OrderByDescending(p => p.Date)
                .Skip((pageNumber - 1)*POSTS_ON_PAGE)
                .Take(POSTS_ON_PAGE);
        }

        // PUT: api/Blogs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBlog(int id, Blog blog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != blog.BlogId)
            {
                return BadRequest();
            }

            db.Entry(blog).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Blogs
        [ResponseType(typeof(Blog))]
        public IHttpActionResult PostBlog(Blog blog)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Blogs.Add(blog);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = blog.BlogId }, blog);
        }

        // DELETE: api/Blogs/5
        [ResponseType(typeof(Blog))]
        public IHttpActionResult DeleteBlog(int id)
        {
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return NotFound();
            }

            db.Blogs.Remove(blog);
            db.SaveChanges();

            return Ok(blog);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BlogExists(int id)
        {
            return db.Blogs.Count(e => e.BlogId == id) > 0;
        }
    }
}